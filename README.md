### Python library and command line scripts for outputing data on compatibility between plants (companions = good association = link weight value 1, anti-companions = bad association = value -1, 0 if no known interaction)


Defaut data for plant links weights are from this source: http://permaculture-potager.com/wp-content/uploads/2015/03/tableau-association.xlsx

Associated blog article french): http://permaculture-potager.com/les-associations-de-plantes-1-reduction-de-la-competition/

Plants names are thus in french..

It has been translated to and ods file, and a csv file readable by gephi (csv matrix of links weights). Those data are found in the data/ subdirectory 

### As of April 2018, the ods reading part is not working anymore on my laptop due to boring python2 vs python3 issues.
### I use python 2.7 and gave up for now, the csv reading part should be functionnal




## Installation:

With `pip` from a git repository:

`sudo pip install git+https://framagit.org/robertbiloute/companionet.git`

That will install the `companionet` package and corresponding python methods, together with scripts that can be run from the command line (works on linux debian, didn't test it on other OS's)

Rq: For a local user installation on a machine without root access:

`pip install --user git+https://framagit.org/robertbiloute/companionet.git`

But then the command line scripts may be in some other directory than `/usr/bin`. (`~/.local/bin/` on my laptop)


## Scripts usage:

Scripts that may be run at the OS command line:

- find_links.py:

	- `python find_links.py string1`

		  List companion and anti-companion of plant whose name is given by the string string1

		  Ex: `python find_links.py "tomate"` # Not case-sensitive

	- `python find_links.py string1 string2`

		  Tells wether the 2 plants named by string1 and string2 are companions, anti companions, or if no link is known between them.
			
		  Ex: `python find_links.py "tomate" "poireau"` outputs wether "tomate" and "poireau" are companions, anti-companions or with no known link.

- layout.py

	 
	- usage: layout.py [-h] [-f FILE] [-m METHOD] [-v VERBOSE]

      Simulate best layout for a set of plants / Simule le meilleur placement pour
un ensemble de plantes

      optional arguments:
	  
        -h, --help            show this help message and exit
		
        -f FILE, --file FILE  Take a text file with a list of plant names as theinput plant list for the layout.
		
        -m METHOD, --method METHOD
		
                        String designating the method used to optimize the
                        layout. Possible choices: - "selection"[default]:
                        random layout and selection based on surrounding
                        neighbours - "force": use repulsive and attractive
                        force to move plants on the layout - "bestscore":
                        maximization of layout score through successive
                        swapping of plants and corresponding neighbours.

        -b BOUNDARY, --boundary BOUNDARY

                        Set boundary conditions on the layout grid: "normal" [default] or "periodic"

        -i INIT, --init INIT  

                        Choose init layout type for layout algorithm, "random" [default] or "layer"

        -v VERBOSE, --verbose VERBOSE
  
                        Spit out the logs



- graph_from_list.py: build a graph based on a the list of plants provided, with layout computed by the (gephi-like) Forceatlas2 algorithm.


COPYRIGHT (code, for default data see source above)

       Copyright © 2018 Robert Biloute  License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>.

       This is free software: you are free to change and redistribute it.  There is NO WARRANTY, to the extent permitted by law.
