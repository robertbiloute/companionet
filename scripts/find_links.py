#!/usr/bin/env python
# -*- coding: utf-8 -*-


# A script than can be called from the command line
# usage:
# `python find_links.py string1`
# => List companion and anti-companion of plant whose name is given by the string string1
# Ex: python find_links.py "tomate" # Not case-sensitive
#
# `python find_links.py string1 string2`
# => Tells wether the 2 plants named by string1 and string2 are companions, anti companions, or if no link is known between them.







# COPYRIGHT (code, for default data see source above)
#       Copyright © 2018 Robert Biloute  License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>.
#       This is free software: you are free to change and redistribute it.  There is NO WARRANTY, to the extent permitted by law.



if __name__ == "__main__":
	from companionet import read
	import sys

	M, links_dic, listerow, listecol=read.read_csv()


	if len(sys.argv) == 1: # no parameter on command line
		print "> I would need at least a plant name as parameter on the command line input"

	elif len(sys.argv) == 2: # only one parameter on command line => list of copanoins and anti companions
		read.list_companions(sys.argv[1],M,listerow,listecol)
		read.list_anticompanions(sys.argv[1],M,listerow,listecol)


	elif len(sys.argv) == 3: # 2 parameters on command line => valuer of link weight for those 2 plant names
		if links_dic[sys.argv[1].upper(),sys.argv[2].upper()] == 0:
			print "\n> No known link between "+sys.argv[1].upper()+" and "+sys.argv[2].upper()+"\n"
		if links_dic[sys.argv[1].upper(),sys.argv[2].upper()] > 0:
			print "\n> "+sys.argv[1].upper()+" and "+sys.argv[2].upper()+" are companions\n"
		if links_dic[sys.argv[1].upper(),sys.argv[2].upper()] < 0:
			print "\n> "+sys.argv[1].upper()+" and "+sys.argv[2].upper()+" are anti-companions\n"

