#!/usr/bin/env python
# -*- coding: utf-8 -*-


# A script than can be called from the command line
# usage:
#
#
#
#
#
#
#
#


# COPYRIGHT (code, for default data see source above)
#       Copyright © 2018 Robert Biloute  License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>.
#       This is free software: you are free to change and redistribute it.  There is NO WARRANTY, to the extent permitted by law.

import argparse
#import pandas
import sys
import numpy as np
import matplotlib.pyplot as plt


from companionet import layout as plant_layout
from companionet import read



def simu_layout(plants,dic,method="selection",verbose=True,boundary="normal", init="random"):

	print "\n> Enter the size of the layout grid n plant rows over m plant columns: "
	print "> Entrez la taille de la grille pour le placement des plantes: n lignes de plantes sur m colonnes de plantes: "

	n = int(raw_input("-> n? "))
	m = int(raw_input("-> m? "))

	print "> "+str(n)+"x"+str(m)

	layout = plant_layout.layout(n,m,plants,dic,init=init)


	exit_loop = 0
	fig=None
	while exit_loop == 0:

		layout.refresh(method=method,boundary=boundary)
		score, bad_assoc = layout.score()

		print

		
		if len(bad_assoc) != 0: #or np.product(layout2 == layout) == 0

			if fig != None:
				fig.clear()
			
			fig = layout.plot(fig=fig)
			plt.pause(0.1)


			if verbose:
				print layout.layout #pandas.DataFrame(layout.layout)
				print "\n"

			sstr= ">> Score : "+str(score)+" | "+str(len(bad_assoc))+" bad associations left."+" | "+str(len(bad_assoc))+" mauvaises associations restantes."
			sys.stdout.write("\r")
			sys.stdout.flush()
			sys.stdout.write(sstr)
			if verbose:
				for c in bad_assoc:
					print str(c)+" : "+layout.layout[c[0][0],c[0][1]]+" - "+layout.layout[c[1][0],c[1][1]]

		elif len(bad_assoc) == 0: #or np.product(layout2 == layout) != 0:
			exit_loop = 1

			if fig != None:
				fig.clear()
			
			fig = layout.plot(fig=fig)
			plt.pause(0.1)
			if verbose:
				print layout.layout #pandas.DataFrame(layout.layout)
			print "\n"
			print "> "+str(len(bad_assoc))+" bad associations left."
			print "> "+str(len(bad_assoc))+" mauvaises associations restantes.\n\n"

			print ">>>> Convergence reached with score "+str(score)
			print ">>>> Convergence atteinte avec un score de "+str(score)
			print "\n\n"

			return layout







	
parser = argparse.ArgumentParser(description='Simulate best layout for a set of plants / Simule le meilleur placement pour un ensemble de plantes')

parser.add_argument("-f", "--file", type=str, help="Take a text file with a list of plant names as the input plant list for the layout.")
parser.add_argument("-m", "--method", type=str,default="selection", help="String designating the method used to optimize the layout.\n Possible choices: \n   - \"selection\"[default]: random layout and selection based on surrounding neighbours \n - \"force\": use repulsive and attractive force to move plants on the layout   - \"bestscore\": maximization of layout score through successive swapping of plants and corresponding neighbours.")
parser.add_argument("-b", "--boundary", type=str, default="normal", help="Set boundary conditions on the layout grid: \"normal\" [default] or \"periodic\"")
parser.add_argument("-i", "--init", type=str, default="random", help="Choose init layout type for layout algorithm, \"random\" [default] or \"layer\"")
parser.add_argument("-v", "--verbose", help="Spit out the logs")

args = parser.parse_args()
method = args.method
file_ = args.file
boundary = args.boundary
init = args.init
verbose = args.verbose

print "\n######## Input parameters ########"
print "> method set to "+str(method)
print "> file input set to "+str(file_)
print "> boundary conditions set to "+str(boundary)
print "> verbose set to "+str(verbose)
print "################################## \n"



if __name__ == "__main__":



	#np.set_printoptions(linewidth = 210)




	M, links_dic, listerow, listecol=read.read_csv()
	#M[range(M.shape[0]),range(M.shape[0])] = 1 # unity weight for self link : tends form area of the same plant
	
	#g = read.igraph_from_matrix(M,listerow)

	#l=g.layout_auto(weights=g.es['weight'],maxiter=10000)

	#read.igraph.plot(g,layout=l)

	if file_ == None:
		print "> Enter a list of plants, one after the other (Enter to validate, \"q\" to end list)"
		print "> Entrez une liste de plante, l'une après l'autre (Entrée pour valider une plante, \"q\" pour terminer la liste)"
		print "\n"
		print "> For N plants at once, type \"plant_name*N\", for example \"tomate*3\" adds 3 tomatoes to the list."
		print "> Pour N plantes d'un seul coup, tapez \"nom_de_la_plante*N\", par exemple: \"tomate*3\" ajoute 3 tomates à la liste."
		plants = []
		plant = ""
		i=0
		while plant != "q":
				plant = raw_input("-> Plant "+str(len(plants)+1)+": ")
				if plant.find("*") != -1:
					Nplant = int(plant.split("*")[1])
					plant = plant.split("*")[0]
				else:
					Nplant = 1

				if plant.upper() in listerow:
					for k in range(Nplant):
							plants.append(plant)
				else:
					if plant != "q":
							print "## Cette plante n'est pas dans ma liste / Plant not in my list \##"
					else: 
							print "> Fin de la liste / End of the list <\n"
				i+=1

	else: # read plant list from file
		plants = []
		fid = open(file_)

		temp = fid.readline()
		while temp != "":
			if temp.find("*") != -1:
				Nplant = int(temp.split("*")[1])
				temp = temp.split("*")[0]
			else:
				Nplant = 1

			for i in range(Nplant):
				plants.append(temp.split("\n")[0]) # remove trailing \n
			temp = fid.readline()
		if plants[-1] == '':
			plants.pop(-1)
		if plants[-1] == '\n':
			plants.pop(-1)

		fid.close()

            
	print "\n> "+str(len(plants))+" plants in list."
	print "> Liste : "+str(plants)
	
	M, listerow, listerow = read.select_plants_in_matrix(plants,M,listerow,listerow)
	dic = read.dictionary_from_matrix(M,listerow,listerow)


	PLACED = []
	score_old = np.nan
	go_on = True
	while go_on:

		layout = simu_layout(plants,dic,method=method, verbose=verbose, init=init)

		score_new = layout.score()
		if method == "bestscore" and score_new == score_old or score_new < score_old:
			go_on = False

		score_old = score_new

		placed = list(set(layout.layout.flatten()))
		notplaced = [p for p in plants if p not in placed]

		print "> Plants that have been placed / Plantes qui ont été placées ("+str(len(placed))+"/"+str(len(plants))+"): "
		print placed
		print "\n"

		print "> Plants that have not been placed / Plantes qui n'ont pas été placées ("+str(len(notplaced))+"/"+str(len(plants))+"): "
		print notplaced
		print "\n"

		PLACED.append(placed)

		plt.draw()
		
		if  len(notplaced) == 0:
			go_on = False
			break
		elif len(notplaced) !=0 or go_on == False:
			print("\n> Simuler une autre grille avec les plantes non placées ? (o/n)")
			aa = raw_input("> Run another layout optimization with the group of unplaced plants ? (y/n)")

			if aa == "y" or aa == "Y" or aa == "o" or aa == "O":
				plants = notplaced
			if aa == "n" or aa == "N":
				go_on = False

	
