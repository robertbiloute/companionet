#!/usr/bin/env python
# -*- coding: utf-8 -*-


# A script than can be called from the command line
# usage:
#
#
#
#
#
#
#
#


# COPYRIGHT (code, for default data see source above)
#       Copyright © 2018 Robert Biloute  License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>.
#       This is free software: you are free to change and redistribute it.  There is NO WARRANTY, to the extent permitted by law.

import networkx as nx
from companionet import read
from companionet import layout
from prompt_toolkit import prompt
from prompt_toolkit.validation import Validator, ValidationError
from prompt_toolkit.contrib.completers import WordCompleter
import numpy as np
import community
from matplotlib import cm


# from :https://github.com/alexjh/garden-planner/blob/master/planner/planner_ui.py
class PlantNameValidator(Validator):
    """
    Ensures the plant name is valid
    """

    def __init__(self, names):
        self.names = names

    def validate(self, document):
        text = document.text

        if text.upper() not in self.names and text != '':
            raise ValidationError(message='{} is not a valid plant name'.
                                  format(text))


# from :https://github.com/alexjh/garden-planner/blob/master/planner/planner_ui.py
def get_plant(plant_names):
    """
    Gets a plant to choose
    """

    plant_names_low = [p.lower() for p in plant_names]
    plant_names_up = [p.upper() for p in plant_names]
    plant_names_uplow = plant_names_up + plant_names_low
    completer = WordCompleter(plant_names_uplow)
    validator = PlantNameValidator(plant_names)
    text = prompt(u'Select a plant: (empty string to end the list, empty list to select all plants)\n', completer=completer,
                  validator=validator)
    return text













####################################################################################################################

if __name__ == "__main__":

	M,links_dic, listerow,listecol = read.read_csv()
	plants = []
	ex = False
	while not ex:

		plant = get_plant(listerow) #([unicode(l) for l in listerow])
		if plant != '':
			plants.append(plant)
		else:
			print "\n> end of list"
			ex = True
		print " | ".join(plants)

	if len(plants) == 0:
		plants = listerow

	MM,lr,lc=read.select_plants_in_matrix(plants,M,listecol,listecol)
	MM2 = MM.copy() # copy of the adjacency matrix with weights offset so that they are >= 0
	MM2 = MM2 + 1
	MM2[MM2==2] = 10 # unit positive weights are now = 10


	GG = read.nx_from_matrix(MM,lc)
	GG2 = read.nx_from_matrix(MM2,lc)
	pos=layout.fa2_layout(GG2,lc,Gplot=GG) # layout positions computed based on GG2 (positive weights) and graph plotted based on GG (negative and positive weights)




	# compute partition of nodes
	part = community.best_partition(GG2)
	mmax = 0
	for k in part.keys():
		if part[k] > mmax:
			mmax = part[k]
	mmax = mmax + 1 # nb of communities

	# colors for communities defined by partition
	color_cycle = []
	for i in np.linspace(0,1,mmax):
		color_cycle.append(cm.gist_rainbow(i)) # here colors picked on the "hsv" colormaps


	i = 0
	avgx = 0
	avgy = 0
	for ll in nx.get_node_attributes(GG,'label'): # positions barycenter to be corrected for
		avgx = (avgx*i + pos[ll][0])/(i+1)
		avgy = (avgy*i + pos[ll][1])/(i+1)

		i+=1



	i = 0
	stdx = 0
	stdy = 0
	for ll in nx.get_node_attributes(GG,'label'): # positions spread to be normalized
		stdx = (stdx*i + (pos[ll][0]-avgx)**2)/(i+1)
		stdy = (stdy*i + (pos[ll][1]-avgy)**2)/(i+1)

		i+=1

	Kx = 1000/np.sqrt(stdx) # scale factor for node positions
	Ky = 1000/np.sqrt(stdy)

	# nodes position, size and color
	i = 0
	for ll in nx.get_node_attributes(GG,'label'):
		GG.node[ll]['viz'] = {'position':{'x': (pos[ll][0]-avgx)*Kx, 'y': (pos[ll][1]-avgy)*Ky, 'z' : 0.0}}
		GG.node[ll]['viz']['size'] = 75.0
		GG.node[ll]['viz']['color'] = {'r': int(color_cycle[part[ll]][0]*256),'g':int(color_cycle[part[ll]][1]*256) ,'b':int(color_cycle[part[ll]][2]*256), 'a': int(color_cycle[part[ll]][3]*256)}
		i+=1

	# edges color
	for i in range(GG.number_of_nodes()):
		
		for key in GG.edge[i]:
			w = GG.edge[i][key]['weight']
			if w < 0: # red for negative weight
				GG.edge[i][key]['viz'] = {'color' : {'r':256,'g':0,'b':0}}
			if w > 0: # blue for positive weight
				GG.edge[i][key]['viz'] = {'color' : {'r':0,'g':0,'b':256}}
		



	GG = nx.relabel_gexf_graph(GG)
	print "> writing gexf file..."
	nx.write_gexf(GG,"./test_networkx2.gexf",version="1.2draft")
	# replace namespace "ns0" of gexf file with "viz", this bug is possibly corrected in further version of networkx, but I use an old debian stable version (06/2018)
	fid = open('test_networkx2.gexf','r')
	aa=fid.readlines()
	fid.close()

	fid = open('test_networkx2.gexf','w')
	fid.writelines(''.join(aa).replace("ns0:","viz:"))
	fid.close()
