# -*- coding: utf-8 -*-
import os

import read as read_module
import layout
import community
import numpy as np
import networkx as nx
from matplotlib import cm

path = os.path.dirname(os.path.realpath(__file__))+"/"
#path = "/".join(path.split("/")[:-2])+"/"


def read(filename=path+"data/tableau-association.gexf"):
	"""
	Read gexf file, outputs a graph object
	"""
	return read_module.read_gexf(filename=filename)
	

def write(filename=path+"data/temp_graph.gexf"):
        """
        Writes a .gexf file with simplegexf module. Stored in "data/temp_graph.gexf" if no filename provided.
        """


        fid=open(filename,'r')
        G = simplegexf.xmltodict.parse(fid)

        return G['gexf']['graph']

def add_node(node,G):
	"""
	Add node to graph.
	"""

	G['nodes']['node'].append(node)


def find_node(name,G):
	"""
	Find a node by its label in graph G, outputs this node, or -1 if not found
	"""
	
	for node in G['nodes']['node']:
		if node['@label'] == name:
			return node
	return -1

def gexf_from_list(plants,filename):	  

	M,links_dic, listerow,listecol = read_module.read_csv()
	MM,lr,lc=read_module.select_plants_in_matrix(plants,M,listecol,listecol)

	MM2 = MM.copy() # copy of the adjacency matrix with weights offset so that they are >= 0
	MM2 = MM2 + 1
	MM2[MM2==2] = 10 # unit positive weights are now = 10


	GG = read_module.nx_from_matrix(MM,lc)
	GG2 = read_module.nx_from_matrix(MM2,lc)
	pos=layout.fa2_layout(GG2,lc,Gplot=GG)




	# compute partition of nodes
	part = community.best_partition(GG2)
	mmax = 0
	for k in part.keys():
		if part[k] > mmax:
			mmax = part[k]
	mmax = mmax + 1 # nb of communities

	# colors for communities defined by partition
	color_cycle = []
	for i in np.linspace(0,1,mmax):
		color_cycle.append(cm.gist_rainbow(i)) # here colors picked on the "hsv" colormaps


	i = 0
	avgx = 0
	avgy = 0
	for ll in nx.get_node_attributes(GG,'label'): # positions barycenter to be corrected for
		avgx = (avgx*i + pos[ll][0])/(i+1)
		avgy = (avgy*i + pos[ll][1])/(i+1)

		i+=1



	i = 0
	stdx = 0
	stdy = 0
	for ll in nx.get_node_attributes(GG,'label'): # positions spread to be normalized
		stdx = (stdx*i + (pos[ll][0]-avgx)**2)/(i+1)
		stdy = (stdy*i + (pos[ll][1]-avgy)**2)/(i+1)

		i+=1

	Kx = 1000/np.sqrt(stdx) # scale factor for node positions
	Ky = 1000/np.sqrt(stdy)

	# nodes position, size and color
	i = 0
	for ll in nx.get_node_attributes(GG,'label'):
		GG.node[ll]['viz'] = {'position':{'x': (pos[ll][0]-avgx)*Kx, 'y': (pos[ll][1]-avgy)*Ky, 'z' : 0.0}}
		GG.node[ll]['viz']['size'] = 75.0
		GG.node[ll]['viz']['color'] = {'r': int(color_cycle[part[ll]][0]*256),'g':int(color_cycle[part[ll]][1]*256) ,'b':int(color_cycle[part[ll]][2]*256), 'a': int(color_cycle[part[ll]][3]*256)}
		i+=1

	# edges color
	for i in range(GG.number_of_nodes()):
		
		for key in GG.edge[i]:
			w = GG.edge[i][key]['weight']
			if w < 0: # red for negative weight
				GG.edge[i][key]['viz'] = {'color' : {'r':256,'g':0,'b':0}}
			if w > 0: # blue for positive weight
				GG.edge[i][key]['viz'] = {'color' : {'r':0,'g':0,'b':256}}
		



	GG = nx.relabel_gexf_graph(GG)
	nx.write_gexf(GG,filename+".gexf",version="1.2draft")
	# replace namespace "ns0" of gexf file with "viz", this bug is possibly corrected in further version of networkx, but I use an old debian stable version (06/2018)
	fid = open(filename+'.gexf','r')
	aa=fid.readlines()
	fid.close()

	fid = open(filename+'.gexf','w')
	fid.writelines(''.join(aa).replace("ns0:","viz:"))
	fid.close()
