#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Python library for outputing data on compatibility between plants (companions = good association = link weight value 1, anti-companions = bad association = value -1, 0 if no known interaction)
# Defaut data for plant links weights are from this source: http://permaculture-potager.com/wp-content/uploads/2015/03/tableau-association.xlsx
# Associated blog article french): http://permaculture-potager.com/les-associations-de-plantes-1-reduction-de-la-competition/
# Plants names are thus in french..
# It has been translated to and ods file, and a csv file readable by gephi (csv matrix of links weights). Those data are found in the data/ subdirectory 

# As of April 2018, the ods reading part is not working anymore on my laptop due to boring python3 vs python3 issues.
# I use python 2.7 and gave up for now, the csv reading part should be functionnal



# COPYRIGHT (code, for default data see source above)
#       Copyright © 2018 Robert Biloute  License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>.
#       This is free software: you are free to change and redistribute it.  There is NO WARRANTY, to the extent permitted by law.


# Note on colors used to encode plants link on the original excel file ##################

#In [39]: bleu
#Out[39]: '28864L'

#In [40]: vert
#Out[40]: '45136L'

#In [41]: rouge
#Out[41]: '16711680L'

# sheet[8,8].background_color

##########################################################################################

#import pyoo # pb with python3 B.C 2018/04

import numpy as np
import os
import time
import csv
import simplegexf
import igraph
import sys
import networkx as nx

import pdb

path = os.path.dirname(os.path.realpath(__file__))+"/"
#path = "/".join(path.split("/")[:-2])+"/"

###############################################################################################################################################
############ .ods manipulation based on ald script, does not seem to work anymore dure to pb with pyoo wanting python3 ? so not really tested #
###############################################################################################################################################

def read_ods(filename=path+"data/tableau-association.ods"):
	"""
	Open an ods file through launching openoffice instance via pyoo module, returns a `sheet` object
	"""
	os.system('soffice --accept=\"socket,host=localhost,port=2002;urp;\" --norestore --nologo --nodefault --headless &')

	desktop = pyoo.Desktop('localhost', 2002)

	doc = desktop.open_spreadsheet(filename)

	sheet = doc.sheets[0]

	return sheet


def scan_sheet(filename=path+"data/tableau-association.ods"):
	"""
	open .ods file and scan for list of plants. Outputs `sheet` objetc, list of plant names (rows) and list of plant names (columns)
	"""
	listecol=[]
	listerow=[]


	sheet = read_ods(filename)

	tt = time.localtime()
	Y = str(tt.tm_year)
	M = str(tt.tm_mon)
	D = str(tt.tm_mday)
	h = str(tt.tm_hour)
	m = str(tt.tm_min)
	s = str(tt.tm_sec)

	fid = open(filename+Y+M+D+"_"+h+m+s+".csv",'w')

	kk=0
	for j in range(5,73):
		aa = sheet[7,j].value
		if aa == u'\u0152ILLET':
			aa="OEILLET"
		if kk == 11:
			aa = u'CELERI BRANCHES'
		listecol.append(aa)
		fid.write(";"+"\'"+aa+"\'")
		kk+=1
	fid.write(";\n")

	for i in range(8,76):
		listerow.append("\'"+sheet[i,4].value+"\'")
	listerow[47] = "\"OEILLET\""


	return sheet, listerow, listcol


def ods_to_matrix(filename=path+"data/tableau-association.ods",write_csv=False):
	"""
	Open and reads an ods file, outputs a numpy matrix with links weights in it, possibly writes a corresponding csv file if `write_csv=True` (readable by Gephi)
	"""

	sheet,listrow, listcol = scan_sheet(filename)
	
	if write_csv:
		fid = open(filename+Y+M+D+"_"+h+m+s+".csv",'w')


	Ncol = len(listecol)
	Nrow = len(listerow)


	M = np.zeros((Nrow,Ncol))
	
	if write_csv:

		for j in range(Ncol):
	
			fid.write(";"+"\'"+listecol[j]+"\'")

	
	for i in range(Nrow):
	
		for j in range(Ncol):

			if write_csv:
				if j == 0:
					fid.write(listerow[i]+";")

			v = sheet[i+8,j+5].background_color
			if v == 16711680:
				M[i,j] = -0.1

			if v == 45136:
				M[i,j] = 1

			fid.write(str(M[i,j]))
			if j == Ncol-1:
				fid.write("\n")
			else:
				fid.write(";")
	fid.close()

	return M


######### END .ods  ##############################################################

###############################################################################################################################################
######### csv file with matrix of links between plants ##########################
###############################################################################################################################################

def read_csv(filename=path+"data/tableau-association4.csv"):
	"""
	Reads a csv file that contains the links weights matrix[1] between plants (Gephi compatible)
	Outputs:
	- numpy matrix with links weights -1, 0 or 1
	- links_dic: 2D dictionnary that outputs the link's weight value for a given pair of plants. Ex: `links_dic['carotte','poireau']` gives the link's weight value between carotte and poireau.
	- list of plants (row)
	- list of plants (column) # Rq: in the default data set, columns and rows are the same list of plants 

	[1] A.K.A. adjacency matrix
	"""

	print "\n> Data loaded from csv file: "+filename+"\n"

	M = []
	links_dic = {} # 2D dictionnary for link eight between plant1 and plant2: links_dic['plant1']['plant2'] = -1|0|1
	listerow = []
	with open(filename, 'r') as csvfile:

		spamreader = csv.reader(csvfile, delimiter=';')

		i = -1
		for row in spamreader:
			if i == -1:
				listecol = row
				listecol.pop() # remove first and last, empty string
				listecol.pop(0)
				i+=1
			else:
				# fill the 2D dic
				listerow.append(row[0])
	
				row.pop(0)
				j = 0
				for v in row:
					if float(v) != 0:
						links_dic[listerow[-1].replace("\'",""),listecol[j].replace("\'","")] = float(v)/abs(float(v))
					else:
						links_dic[listerow[-1].replace("\'",""),listecol[j].replace("\'","")] = 0.0

					j+=1

				# fill the future numpy matrix
				M.append(row)
				i+=1

	M = np.array(M,dtype=float)
	M[M<0] = -1 # normalisaton of negative links weight

	# seems the original default data does not give a symetric matrix, for example weight('courgette','tomate') != weight('tomate','courgette')
	# force symetry

	M = M + M.T # add matrix and transpose
	ii = np.where(M!=0)
	M[ii[0],ii[1]] = M[ii[0],ii[1]]/abs(M[ii[0],ii[1]]) # re-normalize non zero terms



	for k in range(len(listecol)):
		listecol[k] = listecol[k].replace("\'","") # remove quote from string name of plant, redundant

	for k in range(len(listerow)):
		listerow[k] = listerow[k].replace("\'","") # remove quote from string name of plant, redundant

	# now symetrize dic
	links_dic = dictionary_from_matrix(M,listerow,listecol)


	return M,links_dic, listerow,listecol


def write_csv_from_matrix(filename,M,listerow,listecol):
	"""
	Write a csv file with data from weight matrix M and list of plants listerow and listecol
	"""

	for i in range(len(listecol)):
		listecol[i] = listecol[i].upper()
	for i in range(len(listerow)):
		listerow[i] = listerow[i].upper()

	fid = open(filename,"w")
	fid.write(";\'"+"\';\'".join(listecol)+"\';\n")

	for i in range(M.shape[0]):
		l = [str(aa) for aa in M[i,:]]
		fid.write("\'"+listerow[i].upper()+"\';"+";".join(l)+"\n")
	fid.close()




############### igraph objects ########################################################
#######################################################################################

## Note on community spinglass: l=g3.community_spinglass(weights=g3.es['weight'],implementation="neg",spins=3)
# plot community-based graph: https://stackoverflow.com/questions/40443030/how-to-plot-community-based-graph-using-igraph-for-python

## community multilevel (no negative weights allowed)
# aa=g3.community_multilevel(weights=list(array(g3.es['weight'])+1))
# aa.membership



## Note on layout:
### force driven:
# l=g3.layout_kamada_kawai(maxiter=10000,kkconst=1000000000000)
#
# l=g3.layout_fruchterman_reingold(weights=g3.es['weight'],repulserad=0.000005,maxiter=10000)
#

## MDS (with optimization on a given distance matrix dist)
# dist=M.copy() # adjacency numpy matrix
# dist[dist==0]=10 # set dist w.r.t. weights
# dist[dist==1]=1
# dist[dist==-1]=100
# l=g3.layout_mds(dist=dist.tolist())

# then: igraph.plot(g3,layout=l).show()


def igraph_from_matrix(M,listerow,plotfig=False):
	"""
	Create an igraph object from the adjacency matrix M and the label (plant) names list listerow
	"""
	g = igraph.Graph()
	g=g.Weighted_Adjacency(list(M),mode=igraph.ADJ_MIN)
	g.vs['label']=listerow

	for e in g.es:
        	if e['weight'] > 0:
            		e['color'] = 'blue'
	        elif e['weight'] < 0:
            		e['color'] = 'red'
	if plotfig:
		igraph.plot(g).show()

	return g


############### End igraph objects ########################################################
#######################################################################################





############### Networkx object ########################################################
#######################################################################################

def nx_from_matrix(M,listecol):
	"""
	outputs a NetworkX graph object from an adjacency (numpy) matrix
	"""

	G = nx.from_numpy_matrix(M)
	# fill in label
#	for i in range(len(listecol)):
#		G.node[listecol[i]] = G.node[i]
#		del G.node[i]
	for i in range(len(listecol)):
		G.node[i]['label'] = listecol[i]

	return G


############### End Networkx object ########################################################
#######################################################################################





def dictionary_from_matrix(M,listerow,listecol):
	"""
	Output a dictionnat given an adjacency matrix M and a list of rows and colums plant names.
	"""


	dic = {}
	for i in range(M.shape[0]):
		for j in range(M.shape[1]):
			dic[listerow[i].upper(),listecol[j].upper()] = M[i,j]

	return dic

def select_plants_in_matrix(plant_list,M,listerow,listecol):
	"""
	Select a list of plant, designated by strings in the input list `plant_list`.
	The plants name of the input weight matrix M are stored in strings lists listerow and listecol.
	A new weight matrix with only the list of plants asked for is outputed, with updated listerow and listecol
	"""

	i_plants = []
	j_plants = []

	for plant in plant_list:
		plant = plant.upper()
		ii = listerow.index(plant)
		if ii != -1:
			i_plants.append(ii)
		jj = listecol.index(plant)
		if jj != -1:
			j_plants.append(jj)

	Mout = np.zeros((len(plant_list),len(plant_list)))
	
	ki = 0
	for i in i_plants:
		kj = 0
		for j in j_plants:
			Mout[ki,kj] = M[i,j]
			kj+=1
		ki+=1
	return Mout, plant_list,plant_list

def select_plants_write_csv(plant_list,filename,datafile=path+"data/tableau-association4.csv"):
	"""
	open csv weight matrix file called `datafile`, select plant in `plant_list`
	write a new csv file called `filename` with the weight matrix of the selection of plants
	"""

	M,links_dic, listerow,listecol = read_csv(datafile)
	Mout, plant_list,plant_list = select_plants_in_matrix(plant_list,M,listerow,listecol)

	write_csv_from_matrix(filename,Mout,plant_list,plant_list)
	print "> Output written to "+filename


def list_companions(plantname,M,listerow,listecol,print_output=True):
	"""
	Outputs a list of companions (weight value > 0) for the plant designated by the string `plantname`.
	The weight values matrix M, list of plant names listerow and listecol, are given by the `read_csv()` method.
	If `print_output == False`, does not print results to stdio, only return a list of plant names (default is True).
	"""

	plantname = plantname.upper()
	companions = []

	if listerow.count(plantname) == 0:
		print "!! Plant not in the list !!"
		print "> List of available plants:"
		print listerow
	else:
		i = listerow.index(plantname)
		if print_output:
			print "\n> List of companions for "+plantname+"\n"

		for j in range(M.shape[1]):
			if M[i,j] == 1:
				companions.append(listecol[j])
				if print_output:
					print "- "+listecol[j]

	return companions


def list_anticompanions(plantname,M,listerow,listecol,print_output=True):
	"""
	Outputs a list of anti-companions (weight value < 0) for the plant designated by the string `plantname`.
	The weight values matrix M, list of plant names listerow and listecol, are given by the `read_csv()` method.
	If `print_output == False`, does not print results to stdio, only return a list of plant names (default is True).
	"""

	plantname = plantname.upper()
	anticompanions = []

	if listerow.count(plantname) == 0:
		print "\n!! Plant not in the list !!"
		print "> List of available plants:"
		print listerow
	else:
		i = listerow.index(plantname)
		if print_output:
			print "\n> List of anti-companions for "+plantname+"\n"

		for j in range(M.shape[1]):
			if M[i,j] == -1:
				anticompanions.append(listecol[j])
				if print_output:
					print "- "+listecol[j]

	return anticompanions


###############################################################################################################################################
########### GEXF file (as outputed by e.g. Gephi) ########################################################
###############################################################################################################################################

def read_gexf(filename=path+"data/tableau-association.gexf"):
	"""
	Reads a .gexf file with simplegexf module and outputs a graph object
	"""


	fid=open(filename,'r')
	G = simplegexf.xmltodict.parse(fid)

	return G['gexf']['graph'] 




#G['gexf']['graph']['nodes']['node'][0]
#G['gexf']['graph']['edges']['edge'][0]
#G['gexf']['graph']['edges']['edge'][0]['source']
#G['gexf']['graph']['edges']['edge'][0]['@source']
#G['gexf']['graph']['edges']['edge'][0]['@target']
