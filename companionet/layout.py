from companionet import read
import numpy as np
import sys
from random import randint

from os import getenv
import matplotlib.pyplot as plt
from matplotlib import colors
import networkx as nx


from fa2 import ForceAtlas2


##################################################################################################
############# ForceAtlas2 force directed algorithm for networkX graph layout #####################
##################################################################################################

def fa2_layout(G,listecol,Gplot=None,Niter=10000,plotfig=False):
	"""
	ouputs positions of nodes for networkX graph G as computed by the forceatlas2 algorithm. Niter is the number of iterations.
	If a graph "Gplot" is given, it will be used for plotting (will defined the link color), while G is used for layout computation.
	"""

	forceatlas2 = ForceAtlas2(
                          # Behavior alternatives
                          outboundAttractionDistribution=False,  # Dissuade hubs
                          linLogMode=False,  # NOT IMPLEMENTED
                          adjustSizes=False,  # Prevent overlap (NOT IMPLEMENTED)
                          edgeWeightInfluence=1,

                          # Performance
                          jitterTolerance=1.0,  # Tolerance
                          barnesHutOptimize=True,
                          barnesHutTheta=1.2,
                          multiThreaded=False,  # NOT IMPLEMENTED

                          # Tuning
                          scalingRatio=1.0,
                          strongGravityMode=True,
                          gravity=1.,

                          # Log
                          verbose=True)

	if Gplot != None:
		Gbis = Gplot
	else:
		Gbis = G

#		positions = forceatlas2.forceatlas2_networkx_layout(Gplot, pos=None, iterations=Niter)
#	else:
	positions = forceatlas2.forceatlas2_networkx_layout(G, pos=None, iterations=Niter)

        labels = {}
        for i in range(len(listecol)):
                labels[i] = listecol[i]


	edge_col_list = []
	for i in range(len(Gbis.edges())):
		w = Gbis.get_edge_data(Gbis.edges()[i][0],Gbis.edges()[i][1])['weight']
		if w > 0:
			edge_col_list.append('b')
		elif w < 0:
			edge_col_list.append('r')

	if plotfig:
		nx.draw_networkx(Gbis, positions, labels=nx.get_node_attributes(Gbis,'label'), font_style='bold',font_size=12,node_color='g',node_size=20, edge_color=edge_col_list) #, cmap=plt.get_cmap('jet'), node_size=50, with_labels=True)
		plt.show()

	return positions


##################################################################################################
############## Layout object for homemade layout algorithm on a grid #############################
##################################################################################################

class layout(object):

	def __init__(self,n,m,plants,dic,init="random"):
		"""
		initialize a layout object with nxm plants on a grid (random initialize position), plant names to be placed in the input list "plants"
		Needs a 2D input dictionnary where dic['plant1','plant2'] = plant1/plant2 link's weight (see read.dictionnary_from_matrix)
		If init == "random", initialize with a random map from the given plant list
		If init == "layer", initialize with plants grouped in layers
		"""

		N = len(plants)

		self.layout = np.zeros((n,m),dtype='S128')

		self.n = n
		self.m = m
		self.plants = plants
		
		for i in range(n):
			for j in range(m):

				if init == "random":
					r = randint(0,N-1)
					self.layout[i,j] = plants[r]
				elif init == "layer":
					frac = (m*(i-1) + j)/(n*m)
					self.layout[i,j] = plants[int(round(frac*(len(plants)-1)))]


		layout.dic = dic



	def refresh(self,method='selection',boundary='normal'):
		"""
		Refresh layout based on a given method (default: "selection", new plant if a bad association axist with the neighbourhood, or "bestscore", optimization of layout score swapping neighbours)

		"""
		
		n = self.n
		
		m = self. m
		


		#layout2 = self.layout.copy()
		score_old, bad_assoc = self.score()

		
		for i in range(n):
			for j in range(m):
				
				plant1 = self.layout[i,j]
				neighbours, ij_list = self.neighbours_list(i,j)

				if method == "smart":
					for ij in bad_assoc:
						k = 0
						Dscore = -1e33 # init
						for plant in self.plants:
							self.layout[ij[0][0],ij[0][1]] = plant
							if self.score()[0]-score_old > Dscore:
								Dscore = self.score()[0]-score_old
								best_plant = plant
							self.layout[ij[0][0],ij[0][1]] = best_plant

							
				elif method == "selection":
					for ij in bad_assoc:
						self.layout[ij[0][0],ij[0][1]] = self.plants[randint(0,len(self.plants)-1)] # new plant

				elif method == "bestscore":
					k=0
					for n in neighbours:
						self.layout[i,j] = n
						self.layout[ij_list[k][0],ij_list[k][1]] = plant1
						score_new, bad_assoc = self.score()
						if score_new <= score_old: # this layout is not better
							self.layout[i,j] =plant1
							self.layout[ij_list[k][0],ij_list[k][1]] = n
							k+=1
						else:
							break

				elif method == "force":
					dx_tot = 0
					dy_tot = 0
					i_range = range(n)
					i_range.pop(i)
					j_range = range(m)
					j_range.pop(j)
					for i2 in i_range:
						for j2 in j_range:
							c = self.dic[plant1.upper(),self.layout[i2,j2].upper()]
							v = np.array([i-i2,j-j2])
							d = np.linalg.norm(v)
							phi = np.angle(v[0]+1j*v[1])
							dx = int(round(np.cos(-phi)))
							dy = int(round(np.sin(-phi)))
							if i+dx < n and j+dy < m:
								plant_temp = self.layout[i+dx,j+dy]
								self.layout[i,j] = plant_temp
								self.layout[i+dx,j+dy] = plant1
							if i+dx < n and j+dy > m:
								plant_temp = self.layout[i+dx,j+dy-m]
								self.layout[i,j] = plant_temp
								self.layout[i+dx,j+dy-m] = plant1
							if i+dx > n and j+dy < m:
								plant_temp = self.layout[i+dx-n,j+dy]
								self.layout[i,j] = plant_temp
								self.layout[i+dx-n,j+dy] = plant1
							if i+dx > n and j+dy > m:
								plant_temp = self.layout[i+dx-n,j+dy-m]
								self.layout[i,j] = plant_temp
								self.layout[i+dx-n,j+dy-m] = plant1



	def neighbours_list(self,i,j,boundary="normal"):
		"""
		Outputs list of 8 close neighbours for plant at position (i,j) on layout, and a list of indices pairs for those neighbours.
		Optional argument `boundary`: default to "normal" (nothing above the edges of the grid), can be set to "periodic" (torus-like boundary conditions: f(boundary)=f(-boundary) for X and Y direction)
		"""
		n = self.n
		m = self.m

		ij_list = []

		plants_near = []
		if i > 0 and j > 0:
			imin = i-1
			jmin = j-1
			plants_near.append(self.layout[imin,jmin])
			ij_list.append([imin,jmin])

		if i ==0 and j == 0:
			plants_near.append(self.layout[-1,-1])
			ij_list.append([-1,-1])

		if i > 0:
			plants_near.append(self.layout[i-1,j])
			ij_list.append([i-1,j])

		if i == 0:
			plants_near.append(self.layout[-1,j])
			ij_list.append([-1,j])


		if j > 0:
			plants_near.append(self.layout[i,j-1])
			ij_list.append([i,j-1])

		if j == 0:
			plants_near.append(self.layout[i,-1])
			ij_list.append([i,-1])



		if i < n-1 and j < m-1:
			imax = i+1
			jmax = j+1
			plants_near.append(self.layout[imax,jmax])
			ij_list.append([imax,jmax])

		if i == n-1 and j == m-1:
			plants_near.append(self.layout[0,0])
			ij_list.append([0,0])


		if i < n-1:
			plants_near.append(self.layout[i+1,j])
			ij_list.append([i+1,j])

		if i == n-1:
			plants_near.append(self.layout[0,j])
			ij_list.append([0,j])


		if j < m-1:
			plants_near.append(self.layout[i,j+1])
			ij_list.append([i,j+1])


		if j == m-1:
			plants_near.append(self.layout[i,0])
			ij_list.append([i,0])


		if i > 0 and j < m-1:
			plants_near.append(self.layout[i-1,j+1])
			ij_list.append([i-1,j+1])


		if i == 0 and j == m-1:
			plants_near.append(self.layout[-1,0])
			ij_list.append([-1,0])


		if i < n-1 and j > 0:
			plants_near.append(self.layout[i+1,j-1])
			ij_list.append([i+1,j-1])


		if i == n-1 and j == 0:
			plants_near.append(self.layout[0,-1])
			ij_list.append([0,-1])



		return plants_near,ij_list



	
	def score(self):
		"""
		Outputs layout score base on the sum of all weights, and a count of remaining bad associations
		"""
		n = self.n
		m = self.m



		score = 0
		count = 0
		bad_assoc = [] # is there any bad neighboring left in this layout ?

		for i in range(n):
			for j in range(m):

				plant1 = self.layout[i,j]
				
				plants_near,ij_list = self.neighbours_list(i,j)
				k = 0
				for plant2 in plants_near:
					if self.dic[plant1.upper(),plant2.upper()] < 0:
						bad_assoc.append([[i,j],ij_list[k]])
					score = score + self.dic[plant1.upper(),plant2.upper()]
					count+=1
					k+=1

		if count == 0:
			count = 1
		return score/count, bad_assoc




	
	def plot(self,fig=None):
		"""
		Plot the plant layout in a figure, possibly nicely..
		If fig == None, create a new figure, else use the existing figure with handle "fig".
		Returns a handle to  the figure

		"""
		
		n = self.n
		
		m = self.m
		

		if fig == None:
			fig = plt.figure(figsize = (m,n))
		else:
			plt.figure(fig.number)


		
		color_cycle = ['red','blue','green','magenta','orange','pink','olive','gray','cyan','brown','purple']
		L = len(list(set(self.layout.flatten()))) - len(color_cycle)

		q = len(self.plants)//len(color_cycle) #len(set(self.layout.flatten()))//len(color_cycle)
		r = np.remainder(len(self.plants),len(color_cycle)) #np.remainder(len(set(self.layout.flatten())),len(color_cycle))

		color_cycle = color_cycle*q + color_cycle[:r]
		layout_int = np.zeros(self.layout.shape)
		for i in range(n):
			for j in range(m):
				ii = list(set(self.layout.flatten())).index(self.layout[i,j])
				#if ii+1 > len(color_cycle):
				#	ii_col = ii - len(color_cycle)
				#else:
				#	ii_col = ii
				plt.text(j-0.5,i,self.layout[i,j].replace(" ","\n")) #,bbox=dict(facecolor=color_cycle[ii], alpha=0.5))
				layout_int[i,j] = ii
		#plt.xlim(0,n)
		#plt.ylim(0,m)
		#plt.figure()


		ii = []
		for p in list(set(self.layout.flatten())):
			ii.append(self.plants.index(p))
			
		color_cycle2 = []
		for iii in ii:
			color_cycle2.append(color_cycle[iii])

		cmap = colors.ListedColormap(color_cycle2)

		plt.imshow(layout_int,interpolation='None',cmap=cmap)
		plt.title("Score = "+str(self.score()[0]))
		plt.axis('off')
		if len(fig.axes)<2:
			cbar = plt.colorbar()
			cbar.set_ticks(list(np.arange(len(list(set(self.layout.flatten()))))))
			cbar.set_ticklabels(list(set(self.layout.flatten())))
		plt.draw()

		

		return fig


