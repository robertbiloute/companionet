from flask import Flask, redirect
from flask import render_template
from flask import request
from flask import Flask, session
from flask.ext.session import Session

from companionet import gexf
import flask
import os
import numpy as np

app = Flask(__name__)
SESSION_TYPE = 'filesystem'
app.config.from_object(__name__)
Session(app)

@app.route('/')
def index(plants=[],PlantList=[]):

    fid = open('all_plants.txt')
    PlantList = fid.readlines()
    fid.close()

    return render_template('test_flask.html',plants=plants,PlantList=PlantList)


@app.route('/', methods=['POST'])
def handle_data(plants=[],PlantList=[]):

    plant = request.form['plant']
    plants.append(plant)
    session['plants'] = plants
    fid = open('all_plants.txt')
    PlantList = fid.readlines()
    fid.close()

    return render_template('test_flask.html',plants=plants,PlantList=PlantList)

@app.route('/pouet', methods=['POST'])
def submit_list():
#    plants = flask.g.plants #getattr(flask.g, 'plants', None)
    plants = session.get('plants','Not Set')
    fid = open('all_plants.txt')
    PlantList = fid.readlines()
    fid.close()
    filename = str(int(np.random.rand()*1e6))
    gexf.gexf_from_list(plants,filename)
    os.system('mv '+filename+'.gexf /var/www/gexf-js')
    return redirect("http://localhost/gexf-js/index.html#"+filename+".gexf")
