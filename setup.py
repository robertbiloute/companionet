from setuptools import setup, find_packages

setup(name='companionet',
      version='0.1',
      description ='python library and scripts for listing links between plants (companion / anti-companion / not known or neutral)',
      url='',
      author='R. Biloute',
      author_email='robert.biloute@posteo.net',
      license='GPL',
      packages=find_packages(),
      package_data = {'companionet':['data/*','web/*.py','web/*.txt','web/templates/*.html','web/gexf-js/*.gexf','web/gexf-js/*.html','web/gexf-js/img/*','web/gexf-js/js/*','web/gexf-js/styles/*']},
      #data_files = [('data',['data/tableau-association4.csv','data/tableau-association.gexf', 'data/tableau-association.ods'])],
      install_requires=['simplegexf','networkx==1.11','fa2','prompt_toolkit<=1.0.9','python-louvain','flask>=0.12.1','Flask-Session'], #pandas],
      scripts=['scripts/find_links.py','scripts/layout.py','scripts/graph_from_list.py'],
      zip_safe=False,
      include_package_data=True)

